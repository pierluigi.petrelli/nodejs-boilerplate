module.exports = {
  semi: true, // Omit semicolons at the end of statements
  singleQuote: true, // Use single quotes instead of double quotes
  arrowParens: "avoid", // Omit parens when possible. Example: x => x
  trailingComma: "all", // Print trailing commas wherever possible when multi-line
  bracketSpacing: true, // Print spaces between brackets in object literals
  bracketSameLine: false, // Put the `>` of a multi-line JSX element at the end of the last line instead of being alone on the next line
  tabWidth: 2, // Specify the number of spaces per indentation-level
  printWidth: 80 // Specify the line length that the printer will wrap on
};
